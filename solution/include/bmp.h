//
// Created by Ilya @yurnerox Rakin on 26.10.22.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"

#include <stdint.h>
#include <stdio.h>


struct __attribute__((packed)) bmp_header;

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_FILE
};

enum write_status to_bmp(FILE *out, struct image const *img);

#endif //IMAGE_TRANSFORMER_BMP_H
