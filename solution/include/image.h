//
// Created by Ilya @Ilya @yurnerox Rakin Rakin on 26.10.22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height);

void free_image(struct image *img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
