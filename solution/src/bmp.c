//
// Created by Ilya @yurnerox Rakin on 26.10.22.
//
#include "bmp.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


uint64_t static get_padding(uint64_t width) {
    uint64_t padding = 0;
    if (width % 4 != 0) {
        padding = 4 - (width * 3) % 4;
    }
    return padding;
}

static struct bmp_header make_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    uint64_t padding = get_padding(width);
    header.bfType = 0x4D42;
    header.bfileSize = 14 + 40 + (width * 3 + padding) * height;
    header.bfReserved = 0;
    header.bOffBits = 14 + 40;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (width * 3 + padding) * height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

static bool check_bfType(uint16_t bfType) {
    return bfType == 0x4D42;
}

static bool check_biBitCount(uint16_t biBitCount) {
    return biBitCount == 24;
}

static bool check_biCompression(uint32_t biCompression) {
    return biCompression == 0;
}

static void cant_read(struct image *img, FILE *in) {
    if (ferror(in)) {
        free_image(img);
        img->data = NULL;
    }
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_FILE;
    }
    if (!check_bfType(header.bfType)) {
        return READ_INVALID_SIGNATURE;
    }
    if (!check_biBitCount(header.biBitCount)) {
        return READ_INVALID_BITS;
    }
    if (!check_biCompression(header.biCompression)) {
        return READ_INVALID_HEADER;
    }
    *img = create_image(header.biWidth, header.biHeight);
    if (img->data == NULL) {
        return READ_INVALID_FILE;
    }
    uint64_t padding = get_padding(img->width);
    uint8_t padding_bytes[3] = {0, 0, 0};
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            if (fread(&img->data[i * img->width + j], sizeof(struct pixel), 1, in) != 1) {
                cant_read(img, in);
                return READ_INVALID_FILE;
            }
        }
        if (fread(padding_bytes, sizeof(uint8_t), padding, in) != padding) {
            cant_read(img, in);
            return READ_INVALID_FILE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = make_header(img->width, img->height);
    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_INVALID_FILE;
    }
    uint64_t width = img->width;
    uint64_t height = img->height;
    uint64_t padding = get_padding(width);
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            if (fwrite(&img->data[i * width + j], sizeof(struct pixel), 1, out) != 1) {
                return WRITE_INVALID_FILE;
            }
        }
        if (fwrite(&padding, sizeof(uint8_t), padding, out) != padding) {
            return WRITE_INVALID_FILE;
        }
    }
    return WRITE_OK;
}
