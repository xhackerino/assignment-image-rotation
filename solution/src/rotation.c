//
// Created by Ilya @yurnerox Rakin on 26.10.22.
//
#include "image.h"
#include <stddef.h>


struct pixel get_pixel(const struct image *img, size_t x, size_t y) {
    return img->data[y * img->width + x];
}

void set_pixel(struct image *img, size_t x, size_t y, struct pixel pxl) {
    img->data[y * img->width + x] = pxl;
}

struct image rotate_90(const struct image *img) {
    struct image new_image = create_image(img->height, img->width);
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            set_pixel(&new_image, img->height - i - 1, j, get_pixel(img, j, i));
        }
    }
    return new_image;
}


